<?php

/**
 * @file
 * Views hook implementations.
 */

/**
 * Implementation of hook_views_data().
 */
function viewsapi_example_views_data() {
  $data = array();

  $data['viewsapi_example_person'] = array();
  $data['viewsapi_example_person']['table'] = array('group' => t('Person'));

  // This tells Views that the table is an original source of data and doesn't
  // depend on other tables.
  $data['viewsapi_example_person']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Person'),
    'help' => t('A person.'),
    'weight' => '10',
  );

  // Relate the uid field to the users table.
  // This allows us to join from the users table.
  $data['viewsapi_example_person']['table']['join'] = array(
    'users' => array(
      'field' => 'uid',
      'left_field' => 'uid',
    ),
    'node' => array(
      'field' => 'uid',
      'left_field' => 'uid',
    ),
    'person' => array(
      'field' => 'parent',
      'left_field' => 'id',
    ),
  );

  $data['viewsapi_example_person']['uid'] = array(
    'title' => t('Drupal user id'),
    'help' => t('The Drupal user id of the person.'),
    // This relationship is for joining from the viewsapi_example_person table.
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'relationship field' => 'uid',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('Drupal user id'),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['viewsapi_example_person']['id'] = array(
    'title' => t('Person ID'),
    'help' => t('The person ID of the person.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['viewsapi_example_person']['first_name'] = array(
    'real field' => 'nameFirst',
    'title' => t('First Name'),
    'help' => t('The first name of the person.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['viewsapi_example_person']['last_name'] = array(
    'title' => t('Last Name'),
    'help' => t('The last name of the person.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['viewsapi_example_person']['birthday'] = array(
    'title' => t('Birthday'),
    'help' => t('The birthday of a person.'),
    // Note this handler comes from the date_api module.
    'field' => array(
      'handler' => 'date_api_handler_field_date_iso',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  $data['viewsapi_example_person']['parent'] = array(
    'title' => t('Parent'),
    'help' => t('The parent of the person.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    // This relationship does a join to our own table.
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'relationship field' => 'parent',
      'base' => 'viewsapi_example_person',
      'base field' => 'id',
      'label' => t('Parent id'),
      'field' => array(
        'handler' => 'views_handler_field',
      ),
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data['viewsapi_example_person']['parent_type'] = array(
    'title' => t('Parent Type'),
    'help' => t('The parent type of a parent.'),
    'field' => array(
      'handler' => 'viewsapi_example_handler_field_parent_type',
    ),
    'filter' => array(
      'handler' => 'viewsapi_example_handler_filter_parent_type',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_views_handlers().
 */
function viewsapi_example_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'viewsapi_example') . '/views',
    ),
    'handlers' => array(
      'viewsapi_example_handler_field_parent_type' => array(
        'parent' => 'views_handler_field',
      ),
      'viewsapi_example_handler_filter_parent_type' => array(
        'parent' => 'views_handler_filter_in_operator',
      ),
    ),
  );
}

