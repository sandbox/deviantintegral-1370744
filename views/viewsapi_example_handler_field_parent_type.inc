<?php

/**
 * Field handler for parent types.
 */

class viewsapi_example_handler_field_parent_type extends views_handler_field {

  /**
   * When displaying the item, we want the human-friendly version.
   */
  function render($values) {
    switch ($values->{$this->field_alias}) {
      case 'M':
        return t('Mother');
      case 'F':
        return t('Father');
      default:
        return t('Unknown');
    }
  }
}

