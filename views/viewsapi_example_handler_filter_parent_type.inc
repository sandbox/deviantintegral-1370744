<?php

/**
 * @class
 * Add a filter to allow for radio selections of parent types.
 */
class viewsapi_example_handler_filter_parent_type extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t("Parent type");
      $this->value_options = array(
        'M' => t('Mother'),
        'F' => t('Father'),
      );
    }
  }
}

